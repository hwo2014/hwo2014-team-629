package noobbot.car;

/**
 * Created by tim on 23/04/2014.
 */
public class CrashData {
    public double slipAngle;
    public double speed;
    public double pieceIndex;

    public CrashData(double slipAngle, double speed, double pieceIndex){
        this.slipAngle = Math.abs(slipAngle);
        this.speed = speed;
        this.pieceIndex = pieceIndex;
    }
}
