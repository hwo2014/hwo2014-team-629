package noobbot.car;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

/**
 * Created by chriss on 4/20/14.
 *
 * This class stores a list of all of the cars currently on the track
 *
 */
public class Cars {

    private ArrayList<Object> dataList;

    private ArrayList<Car> carList;

    private ArrayList<LinkedTreeMap<String, Object>> carData = null;


    /**
     * Blank constructor
     */
    public Cars() {
        carList = new ArrayList<Car>();
        dataList = new ArrayList<Object>();
    }

    /**
     * Update the configuration of the cars
     * If the cars have not yet been initialised, a separate method will be called which initialises
     * the cars with their name and colour, otherwise the name and colour are used to find the car in
     * the carList. Once the car is found, it's information is updated.
     * @param cars The data for all cars
     */
    public void updateCars(ArrayList<LinkedTreeMap<String, Object>> cars) {
        carData = cars;

        //System.out.println("Car data: " + data);
        if (carList.size() == 0) {
            System.out.println("Creating new car list");
            initialiseCars();
        }

        int carIndex = 0;
        for (LinkedTreeMap<String, Object> car : carData) {
            LinkedTreeMap<String, Object> carDataMap = car;
            LinkedTreeMap<String, Object> carIdMap = (LinkedTreeMap<String, Object>) carDataMap.get("id");

            String carName = (String) carIdMap.get("name");
            String carColour = (String) carIdMap.get("color");

            carIndex = findCarIndex(carName, carColour);
            //System.out.println("Found car with name: " + carName + ", and colour: " + carColour + ", at index: " + carIndex);
            carList.get(carIndex).setSlipAngle(Math.abs((double) carDataMap.get("angle")));
            //carList.get(carIndex).setLap((int) carDataMap.get("lap"));

            LinkedTreeMap<String, Object> carPiecePositionMap = (LinkedTreeMap<String, Object>) carDataMap.get("piecePosition");
            carList.get(carIndex).setCurrentPieceIndex((int) Math.round((double) carPiecePositionMap.get("pieceIndex")));
            carList.get(carIndex).setPreviousInPieceDistance(carList.get(carIndex).getInPieceDistance());
            carList.get(carIndex).setInPieceDistance((double) carPiecePositionMap.get("inPieceDistance"));

            LinkedTreeMap<String, Object> laneInfoMap = (LinkedTreeMap<String, Object>) carPiecePositionMap.get("lane");

            carList.get(carIndex).setStartLaneIndex((double) laneInfoMap.get("startLaneIndex"));
            carList.get(carIndex).setEndLaneIndex((double) laneInfoMap.get("endLaneIndex"));
        }
    }

    public void initialiseCars(ArrayList<LinkedTreeMap<String, Object>> cars){
        carData = cars;
        initialiseCars();
    }
    /**
     * This method will initialise a list of Car objects, it will only initialise the name and id
     * for each car
     */
    private void initialiseCars() {
        for (LinkedTreeMap<String, Object> carInfo: carData){
            LinkedTreeMap<String, Object> carIdMap = (LinkedTreeMap<String, Object>) carInfo.get("id");

            Car car = new Car();
            car.setName((String) carIdMap.get("name"));
            car.setColour((String) carIdMap.get("color"));
            System.out.println("Adding car: " + car.toString());

            carList.add(car);
        }
    }

    /**
     * Given a car name and colour, this method will find where the car is in the carList
     * @param name The name of the car
     * @param colour The colour of the car
     * @return The index of the car in the carList
     */
    public int findCarIndex(String name, String colour) {
        int indexToReturn = 0;
        for (int i = 0; i < carList.size(); i++) {
            if (carList.get(i).getName().equals(name) && carList.get(i).getColour().equals(colour)) {
                indexToReturn = i;
                break;
            }
        }
        return indexToReturn;
    }

    /**
     * Find and return a specific car object based on name and colour
     * Uses the findCarIndex method to get the car object
     * @param name The name of the car
     * @param colour The colour of the car
     * @return The car object which has the same name and colour as the parameters passed
     */
    public Car getCar(String name, String colour) {
        return carList.get(findCarIndex(name, colour));
    }
}
