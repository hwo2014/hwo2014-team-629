package noobbot.car;

import noobbot.*;

import java.util.ArrayList;

/**
 * Created by chriss on 4/20/14.
 *
 * Programatic representation of a Car on the race track.
 *
 * This class will store all the current information about a particular car. This includes
 * everything that the server sends back in the carPositions message.
 *
 * Car data will be sent through the constructor for the method, the parseCarInformation method
 * will then parse the data and populate the relevant fields within the class.
 *
 */
public class Car {

    public static final double MAX_THROTTLE = 1.0;
    public static final double MIN_THROTTLE = 0.0;

    //basic data
    private String name;
    private String colour;

    //race data
    private double slipAngle;
    private int currentPieceIndex;
    private double previousInPieceDistance = -1;
    private double inPieceDistance = 0;

    private double previousPieceDistance;

    //track, lap and lane data
    private Track track;
    private int lap;
    private double startLaneIndex;
    private double endLaneIndex;

    //throttle and speed data
    private double currentThrottle = 0;
    private double previousSpeed = 0;
    private double currentSpeed = 0;

    //turbo data
    private boolean turboAvailable = false;
    private int turboTicks = 0;

    //race state data
    private Main.RaceState raceState = Main.RaceState.RACE_SOLO;

    //race coefficients etc
    private ArrayList<Double> accelerationData; //store speed, index is time
    private ArrayList<Double> decelerationData; //store speed, index is time
    private double decelerationPerTick = 100.0;
    private double MAX_DRIFTING_ANGLE = 70.0;
    private double crashSpeed = 0.0;
    private ArrayList<CrashData> crashLog;

    private boolean accelerationDataSet = false;
    private boolean decelerationDataSet = false;
    //private boolean crashDataSet = false;


    /**
     * Constructor to create a new car
     * @param name The name of the driver of the car
     * @param colour The colour of the car
     */
    public Car(String name, String colour) {
        this();
        this.name = name;
        this.colour = colour;
    }

    /**
     * Blank constructor
     */
    public Car() {
        accelerationData = new ArrayList<>();
        decelerationData = new ArrayList<>();
        crashLog = new ArrayList<>();
    }

    /**
     * This is where we do all our race logic
     * @return
     */
    public double getThrottleResponse(){
        double throttleResponse = 0.0d;

        currentSpeed = calculateCurrentSpeed();

        switch(raceState){
            case QUALIFICATION:
                break;
            case RACE_SOLO:
                throttleResponse = raceSolo();
                break;
            case RACE_CARS:
                break;
            default:
                break;
        }

        System.out.println("Tick data -  Current Speed: " + currentSpeed + ", Current Throttle: " + throttleResponse + ", Track Piece: " + currentPieceIndex + ", Current Angle: " + Math.abs(slipAngle));
        return throttleResponse;
    }

    private boolean raceConditionsSet(){
        return accelerationDataSet && decelerationDataSet;
    }

    /**
     * All logic for a solo race will go here
     * This assumes a warmup lap for getting the 'feel' of a track
     * @return
     */
    private double raceSolo(){
        double throttleResponse = 0.0d;

        if (!raceConditionsSet()){
            //race conditions need gathering...
            throttleResponse = gatherRaceConditions();
        } else {
            //we have the race conditions, so lets race!
            throttleResponse = raceFastest();
        }
        return throttleResponse;
    }

    public String toString(){
        return "Car Name: " + this.name + ", Car Colour: " + this.colour;
    }

    /**
     * Method here to calculate all our coefficients etc
     * Acceleration
     * Deceleration
     * Max cornering angle before we crash (abs value)
     * Corner speed vs angle of crash
     *
     * @return
     */
    private double gatherRaceConditions(){
        double throttleResponse = 0.0d;

        if (!accelerationDataSet){
            throttleResponse = collectAccelerationData();
        } else if (!decelerationDataSet){
            throttleResponse = collectDecelerationData();
        }
        // else if (!crashDataSet){
        //    throttleResponse = collectCrashData();
        //}
        return throttleResponse;
    }

    private double raceFastest(){
        double throttleResponse = 0.0d;

        TrackPiece currentPiece = track.getPieceAtIndex(currentPieceIndex);
        TrackPiece nextPiece = getNextPieceOfTrack();
        double currentDriftAngle = Math.abs(slipAngle);

        if (currentPiece instanceof StraightPiece) {
            //get data for the upcoming corner, work out max braking zone for 0.0 decel into corner
            int nextCornerIndex = track.getNextCornerIndex(currentPieceIndex);
            BendPiece nextCornerPiece = (BendPiece) track.getPieceAtIndex(nextCornerIndex);
            double idealCornerSpeed = nextCornerPiece.getIdealSpeed();

            if (currentSpeed > idealCornerSpeed) {
                //we're too fast for the upcoming corner, so work out if we can brake in time first
                double speedDelta = currentSpeed - idealCornerSpeed;
                double distanceToDecelerate = getDistanceToDecelerate(currentSpeed, speedDelta);
                double distanceToCorner = track.distanceToPiece(currentPieceIndex, nextCornerIndex, inPieceDistance);

                if (distanceToCorner < distanceToDecelerate + currentSpeed) {
                    //if the distance to corner is less than distance needed to decelerate
                    // + our current speed (distance we'll travel next tick)
                    //we need to brake hard
                    throttleResponse = MIN_THROTTLE;
                } else {
                    //we're far enough away to allow ourselves to keep accelerating
                    throttleResponse = MAX_THROTTLE;
                }

            } else {
                //we're going slower than the next corner speed, so time to put on the POWER
                throttleResponse = MAX_THROTTLE;
            }
        } else if (currentPiece instanceof BendPiece){
            //TODO - add lookup here for next corner with less speed than this, if there is one

            BendPiece bendPiece = (BendPiece) currentPiece;

            if (currentSpeed > bendPiece.getIdealSpeed()){
                throttleResponse = MIN_THROTTLE;
            } else if (nextPiece instanceof StraightPiece) { // Exit corner logic - can we start to accelerate?
                throttleResponse = MAX_THROTTLE;
            } else {
               throttleResponse = bendPiece.getThrottleResponseForDrifting(currentDriftAngle);
            }
        } else {
            System.out.println("ERROR: Unknown track piece type");
        }

        return throttleResponse;
    }

    private double getDistanceToDecelerate(double currentSpeed, double speedDelta) {
        //at the moment return a big val so we always slow down in time
        // v = d/t therefore t = d/v
        // a = (v1 - v2) / t
        // t = (v1 - v2) / a
        // therefore: d = ((v1 - v2) / a) v
        double distance = (speedDelta / decelerationPerTick) * currentSpeed;
        return distance;
    }

    public boolean goTurbo(){
        //TODO - track should calc optimal turbo zone and use it here
        return (turboAvailable &&
                track.getPieceAtIndex(currentPieceIndex + 1) instanceof StraightPiece &&
                track.getPieceAtIndex(currentPieceIndex + 2) instanceof StraightPiece &&
                track.getPieceAtIndex(currentPieceIndex + 3) instanceof StraightPiece);
    }

    public double calculateCurrentSpeed() {
        double distanceTravelled;
        if (inPieceDistance >= previousInPieceDistance) {
            distanceTravelled = inPieceDistance - previousInPieceDistance;
        } else {
            distanceTravelled = track.getPieceAtIndex(currentPieceIndex - 1).getLength() - previousInPieceDistance + inPieceDistance;
        }

        return distanceTravelled;
    }

    /**
     * Constant acceleration for x ticks, log speed
     * @return
     */
    private double collectAccelerationData(){
        accelerationData.add(currentSpeed);
        if (accelerationData.size() >= 60){
            accelerationDataSet = true;
        }

        return MAX_THROTTLE;
    }

    /**
     * Decelerate for x ticks, log speed
     * @return
     */
    private double collectDecelerationData(){
        decelerationData.add(currentSpeed);
        if (decelerationData.size() >= 60){
            decelerationDataSet = true;
            //TODO - implement extrapolation here as decel is NOT linear
            decelerationPerTick = decelerationData.get(0) - decelerationData.get(1);
        }

        return MIN_THROTTLE;
    }

    /**
     * Max speed until crash data is recorded
     * @return
     */
//    private double collectCrashData(){
//        //if (MAX_DRIFTING_ANGLE != 0.0 && crashSpeed != 0.0){
//        //    crashDataSet = true;
//        //}
//        if (crashLog.size() > 0){
//            crashDataSet = true;
//        }
//
//        return MAX_THROTTLE;
//    }

    /**
     * Log crash data - current speed and slip angle
     */
    public void logCrash(){
        crashLog.add(new CrashData(slipAngle, currentSpeed, currentPieceIndex));

        if (slipAngle < MAX_DRIFTING_ANGLE){
            MAX_DRIFTING_ANGLE = slipAngle;
        }
    }

    // ==============================================================================
    // Getters and Setters

    /**
     * Get the current piece that the car is in
     * @return The index of the current piece the car is in
     */
    public int getCurrentPieceIndex() {
        return currentPieceIndex;
    }

    /**
     * Set the current piece that the car is in
     * @param currentPieceIndex The index of the piece that the car is currently in
     */
    public void setCurrentPieceIndex(int currentPieceIndex) {
        this.currentPieceIndex = currentPieceIndex;
    }

    /**
     * Get the distance that the car has travelled through a piece
     * @return The distance the car has travelled thorough it's current piece
     */
    public double getInPieceDistance() {
        return inPieceDistance;
    }

    /**
     * Set the inPieceDistance
     * @param inPieceDistance The distance that the car has travelled through it's current piece
     */
    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getSlipAngle() {
        return slipAngle;
    }

    public void setSlipAngle(double slipAngle) {
        this.slipAngle = slipAngle;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public double getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(double startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public double getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(double endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

    public double getPreviousInPieceDistance() {
        return previousInPieceDistance;
    }

    public void setPreviousInPieceDistance(double previousInPieceDistance) {
        this.previousInPieceDistance = previousInPieceDistance;
    }

    public double getCurrentThrottle() {
        return currentThrottle;
    }

    public void setCurrentThrottle(double currentThrottle) {
        this.currentThrottle = currentThrottle;
    }

    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public void setTurboAvailable(boolean turboAvailable) {
        this.turboAvailable = turboAvailable;
        System.out.println("TURBO SET TO " + this.turboAvailable);
    }

    public void setTurboTicks(int turboTicks) {
        this.turboTicks = turboTicks;
    }

    public void setTrack(Track track){
        this.track = track;
    }

    public void setRaceState(Main.RaceState raceState) {
        this.raceState = raceState;
    }

    public TrackPiece getNextPieceOfTrack() {
        return track.getPieceAtIndex(currentPieceIndex + 1);
    }

    // End of Getters and Setters
    //========================================================================


    //TODO - delete once reviewed by Chris
    /**
     * REMOVED - I think ticks are constant, 2ms apart - measuring the time between them is causing lag and inconsistent calcs imo
     *
     * Calculate the current speed of the car
     * @param track The current track we are racing on

    public void calculateCurrentSpeed(Track track) {
    double distanceTravelled;
    if (inPieceDistance >= previousInPieceDistance) {
    distanceTravelled = inPieceDistance - previousInPieceDistance;
    } else {
    distanceTravelled = (track.getPieceAtIndex(currentPieceIndex - 1).getLength() * (1 - (previousInPieceDistance / 100))) + inPieceDistance;
    }

    if (distanceTravelled == 0 && timeSinceLastTick == 0) {
    currentSpeed = 0;
    } else if (timeSinceLastTick != 0) {
    currentSpeed = distanceTravelled / timeSinceLastTick;
    }
    }
     **/
}
