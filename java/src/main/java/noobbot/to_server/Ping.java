package noobbot.to_server;

public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}