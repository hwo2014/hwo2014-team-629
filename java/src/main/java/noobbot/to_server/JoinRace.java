package noobbot.to_server;

import com.google.gson.internal.LinkedTreeMap;

public class JoinRace extends SendMsg {

    public final LinkedTreeMap<String, Object> botId = new LinkedTreeMap<String, Object>();
    public final String trackName;
    public final int carCount;

    public JoinRace(final String name, final String key, final String trackName) {
        botId.put("name", name);
        botId.put("key", key);
        this.trackName = trackName;
        this.carCount = 1;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}