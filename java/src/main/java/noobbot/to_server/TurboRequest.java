package noobbot.to_server;

/**
 * Created by tim on 22/04/2014.
 */
public class TurboRequest extends SendMsg{

        private String data;

        public TurboRequest(String data) {
            this.data = data;
            System.out.println("New turbo requested");
        }

        @Override
        protected Object msgData() {
            return data;
        }

        @Override
        protected String msgType() {
            return "turbo";
        }
}
