package noobbot;

/**
 * Created by chriss on 4/18/14.
 *
 * This is the super class for pieces that make up the track
 *
 * The only thing stored here is whether the piece has a laneSwitch
 * in or not as this is the only common field between both pieces.
 *
 */
public abstract class TrackPiece {

    private boolean laneSwitch;

    /**
     * Constructor
     * @param laneSwitch Whether there is a lane switch on this piece
     */
    public TrackPiece(boolean laneSwitch) {
        this.laneSwitch = laneSwitch;
    }

    /**
     * Default constructor for pieces that do not contain a lane switch
     */
    public TrackPiece() {
        this.laneSwitch = false;
    }

    /**
     * Getter to check whether there is a lane switch in the piece
     * @return Whether there is a lane switch or not
     */
    public boolean isLaneSwitch() {
        return laneSwitch;
    }

    /**
     * Setter for the laneSwitch parameter
     * @param laneSwitch Whether there is a lane switch on this piece
     */
    public void setLaneSwitch(boolean laneSwitch) {
        this.laneSwitch = laneSwitch;
    }

    public abstract double getLength();

    public abstract void setLength(double length);

    public abstract double getIdealSpeed();

    public abstract void setIdealSpeed(double idealSpeed);

    public abstract boolean isBend();
}
