package noobbot;

import noobbot.car.Car;

/**
 * Created by chriss on 4/18/14.
 *
 * Represents a piece of the track that makes up a turn on the course
 *
 * In this class the angle and radius of the turn are stored. The length
 * of the turn is calculated during construction using the following formula:
 *
 * (angle / 360) * 2 * PI * radius
 *
 * In addition the ideal speed coefficient for the piece is stored. As this
 * class represents pieces that form part of a turn the coefficient wil not
 * always be one. Currently the coefficient defaults to 0.658 as this was
 * previously used successfully as a constant speed for the car.
 *
 */
public class BendPiece extends TrackPiece {

    private double angle;
    private double radius;

    private double length;
    //current guesstimate for ideal speed on average corner is about 6.5/tick
    private double idealSpeed = 6.5;

    private static final float PI = 3.14159265359f;

    /**
     * Constructor for a piece that makes up a turn
     *
     * @param radius The radius of the turn
     * @param angle The angle of the turn
     * @param laneSwitch Whether a lane switch can take place in this piece
     */
    public BendPiece(double radius, double angle, boolean laneSwitch) {
        super(laneSwitch);
        this.radius = radius;
        this.angle = angle;
        calculateBendLength();
    }

    /**
     * Calculates the length of the turn using the formula listed above
     */
    public void calculateBendLength() {
        double twoPiR = 2 * PI * radius;
        length = (Math.abs(angle) / 360) * twoPiR;
        System.out.println("Angle: " + angle + ", Radius: " + radius + ", Length: " + length);
    }

    /**
     * Getter for the angle of the turn
     * @return The angle of the turn
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Setter for the angle of the turn
     * @param angle The angle of the turn
     */
    public void setAngle(double angle) {
        this.angle = angle;
    }

    /**
     * Getter for the radius of the turn
     * @return The radius of the turn
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Setter for the radius of the turn
     * @param radius The radius of the turn
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Getter for the length of the turn
     * @return The length of the turn
     */
    public double getLength() {
        if (length < 0) {
            return 0 - length;
        }
        return length;
    }

    /**
     * Setter for the length of the turn
     * @param length The length of the turn
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Get ideal speed
     * @return ideal speed
     */
    public double getIdealSpeed() {
        calculateIdealSpeed();
        return idealSpeed;
    }

    /**
     * Calculate the ideal speed coefficient for the bend
     */
    private void calculateIdealSpeed() {
        // TODO: Create some function that will calculate the ideal speed coefficient
        if (Math.abs(angle) == 22.5){
            idealSpeed = 20;
        } else if (Math.abs(angle) == 45.0){
            idealSpeed = 6.5;
        } else {
            //play it safe for now, until we have some sort of formula here
            idealSpeed = 6.5;
        }
    }

    /**
     * @param currentDriftAngle of the car on the bend
     * @return how to change the throttle to maximise speed without falling off
     */
    public double getThrottleResponseForDrifting(double currentDriftAngle) {
        double throttleResponse = Car.MAX_THROTTLE;

        if (currentDriftAngle > 40) {
            throttleResponse = 0.1d;
        } else if (currentDriftAngle > 30) {
            throttleResponse = 0.4d;
        } else if (currentDriftAngle > 20) {
            throttleResponse = 0.8d;
        }

        return throttleResponse;
    }



    /**
     * Set the ideal speed for the turn
     * @param idealSpeed The ideal speed for the turn
     */
    public void setIdealSpeed(double idealSpeed) {
        this.idealSpeed = idealSpeed;
    }

    public boolean isBend() {
        return true;
    }
}
