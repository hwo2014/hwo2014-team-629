package noobbot;

/**
 * Created by tim on 19/04/2014.
 */
public class RaceMaths {

    /**
     * Returns the speed per tick of the car
     *
     * @param currentDistance
     * @param lastDistance
     * @return
     */
    private static int getSpeed(int currentDistance, int lastDistance){
        return currentDistance - lastDistance;
    }

    public static int getSpeedPerTick(int currentPieceDistance, int previousPieceDistance, int currentPieceIndex){
        int speed;

        if (currentPieceDistance < previousPieceDistance){
            //TODO - calculate distance into previous piece
            //get previousPieceLength
            //speed = (previousPieceLength - previousPieceDistance) + currentPieceDistance
            speed = currentPieceDistance;
        } else {
            speed = getSpeed(currentPieceDistance, previousPieceDistance);
        }
        return speed;
    }

    /**
     * Calculate acceleration between two points over x number of ticks
     * Negative value indicates deceleration
     * acc = change in speed / time
     * @param currentSpeed
     * @param previousSpeed
     * @return acceleration / deceleration
     */
    public static double calculateAcceleration(int numberOfTicks, int currentSpeed, int previousSpeed){
        int speedDelta = currentSpeed - previousSpeed;
        return speedDelta / numberOfTicks;
    }

    public static double calculateIdealSpeed(){
        return 0;
    }


    /**
     * Basic throttle calculator
     * @param pieceIndex
     * @param track
     * @return
     */
    public static double calculateThrottle(int pieceIndex, Track track){
        double throttle = 0.0;
        TrackPiece currentPiece = track.getPieceAtIndex(pieceIndex);
        TrackPiece nextPiece = track.getNextPiece(pieceIndex + 1);

        if (currentPiece instanceof BendPiece || nextPiece instanceof BendPiece){
            throttle = 0.6;
        } else {
            throttle = 1.0;
        }

        return throttle;
    }
}
