package noobbot;

import java.util.ArrayList;

/**
 * Created by chriss on 4/18/14.
 *
 * Represents a straight piece of the course.
 *
 * The length of the piece is stored in this class. There is also an ideal speed coefficient for
 * the piece. As the straightPiece should always be taken at full throttle the coefficient is 1.0
 *
 */
public class StraightPiece extends TrackPiece {

    private double length;
    private double idealSpeed = 1.0;

    /**
     * Constructor
     *
     * @param length Length of the piece
     * @param laneSwitch Whether a lane switch can take place in this piece
     */
    public StraightPiece(double length, boolean laneSwitch) {
        super(laneSwitch);
        this.length = length;
    }

    /**
     * Getter for the length of the piece
     * @return The length of the piece
     */
    public double getLength() {
        return length;
    }

    /**
     * Setter for the length of the piece
     * @param length The length of the piece
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * Getter for the ideal speed
     * @return The ideal speed coefficient for the piece.
     */
    public double getIdealSpeed() {
        return idealSpeed;
    }

    /**
     * Set the ideal speed coefficient for the piece
     * @param idealSpeed The speed coefficient to set for the piece
     */
    public void setIdealSpeed(double idealSpeed) {
        this.idealSpeed = idealSpeed;
    }

    public boolean isBend() {
        return false;
    }
}
