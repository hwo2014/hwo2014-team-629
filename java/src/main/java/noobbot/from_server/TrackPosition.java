package noobbot.from_server;

import noobbot.from_server.YourCar;

/**
 * Created by tim on 19/04/2014.
 */
public class TrackPosition {

    public YourCar id;
    public double angle;
    public PiecePosition piecePosition;

    public int getPieceIndex(){
        return piecePosition.pieceIndex;
    }
}

/**
 *     "angle": 0.0,
 "piecePosition": {
     "pieceIndex": 0,
     "inPieceDistance": 0.0,
     "lane": {
         "startLaneIndex": 0,
         "endLaneIndex": 0
 },
 "lap": 0
 */
