package noobbot.from_server;

/**
 * Created by tim on 20/04/2014.
 */
public class PiecePosition {

    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;

    public PiecePosition(int pieceIndex, double inPieceDistance){
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
    }
}
