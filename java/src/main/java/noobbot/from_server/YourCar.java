package noobbot.from_server;

public class YourCar {
    public String name;
    public String color;

    public YourCar(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public void printInfo() {
        System.out.println("Car Name: " + name + ", Car Colour: " + color);
    }

    public boolean equals(YourCar comparator){
        return name.equals(comparator.name);
    }
}