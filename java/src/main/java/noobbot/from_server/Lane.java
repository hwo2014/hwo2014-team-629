package noobbot.from_server;

/**
 * Created by tim on 20/04/2014.
 */
public class Lane {

    public int startLaneIndex;
    public int endLaneIndex;

    public Lane(int startLaneIndex, int endLaneIndex){
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
}
