package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.car.Car;
import noobbot.car.Cars;

import java.util.ArrayList;
import java.util.HashMap;

import noobbot.from_server.*;
import noobbot.to_server.*;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main {
	
	private final Gson gson = new Gson();
	private PrintWriter writer;

    private Track track;
    private Cars cars;
    private YourCar car;
    private Car ourCar;
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinRace(botName, botKey, "germany"));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;
        cars = new Cars();
        ourCar = new Car();

        LinkedTreeMap<String, Object> gameInit;
        LinkedTreeMap<String, Object> carPositions;
        LinkedTreeMap<String, Object> race;
        ArrayList<LinkedTreeMap<String, Object>> carData;

        send(join);

        while((line = reader.readLine()) != null) {
        	final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            String messageType = msgFromServer.msgType;

            //TODO - need some more logic here to determine what kind of race we are in
            //this is important because atm car just goes fastest, doesn't know if qual, other cars etc...
        	switch (messageType) {
        		case "yourCar":
                    LinkedTreeMap<String, Object> data = (LinkedTreeMap<String, Object>) msgFromServer.data;
                    car = new YourCar((String)data.get("name"), (String)data.get("color"));
                    car.printInfo();
                    break;
        		case "carPositions":
                    carData = (ArrayList<LinkedTreeMap<String, Object>>) msgFromServer.data;

                    cars.updateCars(carData); // parse car positions, update cars in carList.
                    ourCar = cars.getCar(car.name, car.color);
                    double throttleResponse = ourCar.getThrottleResponse();
                    send(new Throttle(throttleResponse));
                    if (ourCar.goTurbo()){
                        send(new TurboRequest("KICK DAT TURBO ON BRUDDA!" + System.currentTimeMillis()));
                        System.out.println("Turbo sent");
                        ourCar.setTurboAvailable(false);
                    }
                    break;
        		case "gameInit":
                    gameInit = (LinkedTreeMap<String, Object>) msgFromServer.data;
                    race = (LinkedTreeMap<String, Object>) gameInit.get("race");
                    carData = (ArrayList<LinkedTreeMap<String, Object>>) race.get("cars");

        			System.out.println("gameInit");
        			track = new Track((LinkedTreeMap<String, Object>) msgFromServer.data);
                    track.parseTrackData();
                    cars.initialiseCars(carData); // parse car positions, update cars in carList.
                    ourCar = cars.getCar(car.name, car.color);
                    ourCar.setTrack(track);
                    break;
        		case "gameEnd":
        			System.out.println("gameEnd");
        			break;
        		case "tournamentEnd":
        			System.out.println("tournamentEnd");
        			break;
        		case "gameStart":
        			System.out.println("gameStart");
        			break;
                case "join":
                    System.out.println("join");
                    break;
                case "lapFinished":
                    System.out.println("lapFinished");
                    break;
                case "crash":
                    //check it was us that crashed, if so, log it!
                    LinkedTreeMap<String, Object> crashData = (LinkedTreeMap<String, Object>) msgFromServer.data;
                    if (ourCar.getName().equals(crashData.get("name"))){
                        ourCar.logCrash();
                    }
                    System.out.println("crash");
                    break;
                case "spawn":
                    System.out.println("spawn");
                    send(new Throttle(1)); // GO
                    break;
                case "dnf":
                    System.out.println("dnf");
                    break;
                case "finish":
                    System.out.println("finish");
                    break;
                case "turboAvailable":
                    System.out.println("TURBO AVAILABLE HOLY WOW");
                    //DO NOT activate turbo as soon as you get it because crashes happen...
                    //send(new TurboRequest("KICK DAT TURBO ON BRUDDA!"));
                    //TODO - parse turbo properly, we need to monitor timeout I think to make sure we still have it
                    LinkedTreeMap<String, Object> turboData = (LinkedTreeMap<String, Object>) msgFromServer.data;
                    ourCar.setTurboAvailable(true);
                    ourCar.setTurboTicks((int) Math.round((double) turboData.get("turboDurationTicks")));
                    break;
        		default:
        			System.out.println("unknown:" + messageType);
        			send(new Ping());
        			break;
        	}
        }
    }

    /**
     * This is our lovely method for calculating the throttle - basically all AI will spawn from here at the moment
     *
     * @return Throttle val between 0.0 and 1.0
     * @param pieceIndex
     */
    private double calculateThrottle(int pieceIndex) {
        double throttle = 0.0;

        throttle = RaceMaths.calculateThrottle(pieceIndex, track);
        return throttle;
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    /**
     * Parse the wonderful JSON and make some position data
     * Cars are indexed by color (because the spec says that's the important bit)
     * @param msgFromServer the RAW server data - not pre-parsed nonsense please
     * @return
     */
    private HashMap<String, TrackPosition> parsePositions(String msgFromServer){
        HashMap<String, TrackPosition> positions = new HashMap<>();

        TrackPositionWrapper parsedPositions = gson.fromJson(msgFromServer, TrackPositionWrapper.class);

        for (TrackPosition position : parsedPositions.data){
            positions.put(position.id.color, position);
        }

        return positions;
    }

    /**
     * Enum to keep track of the race states we're in
     * RACE_SOLO is a solo race, no qual, just laps
     * QUALIFICATION implies a racing solo, to collect data
     * RACE_CARS means there are other cars we can bump etc
     */
    public enum RaceState{
        RACE_SOLO,
        QUALIFICATION,
        RACE_CARS
    }
}
