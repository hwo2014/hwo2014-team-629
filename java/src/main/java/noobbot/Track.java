package noobbot;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by chriss on 4/18/14.
 *
 * This class stores a programatic representation of the track.
 *
 * Currently all that is stored in this class is a list of all of the pieces
 * that make up the track. This information is extracted from the JSON that
 * is received from the server during gameInit
 *
 */
public class Track {

    private ArrayList<TrackPiece> trackLayout = new ArrayList<TrackPiece>();
    private LinkedTreeMap<String, Object> data;

    /**
     * Constructor
     * @param data This is the data contained within the JSON for gameInit. The value stored
     *             under the "data" key within the JSON
     */
    public Track(LinkedTreeMap data) {
        this.data = data;
    }

    /**
     * Parse the data received from the server during the gameInit phase.
     */
    public void parseTrackData() {

        // Data from the JSON object is returned as a LinkedTreeMap. Each key returns a smaller LinkedTreeMap
        // containing the sub-data. I do this recursively, each time with a different key, until I finally get
        // an ArrayList containing the list of pieces that make up the track
        LinkedTreeMap<String, Object> raceData = (LinkedTreeMap<String, Object>) data.get("race");
        LinkedTreeMap<String, Object> trackData = (LinkedTreeMap<String, Object>) raceData.get("track");

        ArrayList<LinkedTreeMap<String, Object>> pieces = (ArrayList<LinkedTreeMap<String, Object>>) trackData.get("pieces");

        // Loop through all of the pieces and add them to my new trackLayout ArrayList
        // Objects in this list are subclasses of TrackPiece
        for (LinkedTreeMap piece : pieces) {
            // If the length is null then the piece is a curve.
            if ((piece.get("length")) == null) {
                /**
                 * Question here: how to we deal with the case that "switch" is null
                 *                I suppose we would just use more if statements but
                 *                that seems a rather clunky way of doing it.
                 */
                trackLayout.add(new BendPiece((double) piece.get("radius"), (double) piece.get("angle"), false));
            } else {
                trackLayout.add(new StraightPiece((double) piece.get("length"), false));
            }
        }
    }

    /**
     * Get the track piece at the specified index
     * @param index The index of the piece to get
     * @return The TrackPiece at the specified index
     */
    public TrackPiece getPieceAtIndex(int index) {
        int indexToGet = index % trackLayout.size();

        TrackPiece piece;

        /**
         * This is for when the car completes a lap. The speed calculator is looking
         * for the -1th piece. At index 0 this would be out of bounds. At the moment
         * I am just catching this exception and returning the last index of the array
         * as this is the piece that comes before 0 on the track.
         */
        try {
            piece = trackLayout.get(indexToGet);
        } catch (IndexOutOfBoundsException e) {
            piece = trackLayout.get(trackLayout.size() - 1);
        }
        return piece;
    }

    /**
     * Get max number of track pieces
     * @return
     */
    public int getTrackPieceSize(){
        return trackLayout.size();
    }

    /**
     * Get the next piece - wraparound protection
     * @param index
     * @return
     */
    public TrackPiece getNextPiece(int index){
        if (index + 1 >= trackLayout.size()){
            return trackLayout.get(0);
        } else {
            return trackLayout.get(index + 1);
        }
    }

    public int getNextCornerIndex(int pieceIndex) {
        int nextCorner = -1;
        while (nextCorner == -1){
            pieceIndex++;
            pieceIndex = pieceIndex % trackLayout.size();
            if (getPieceAtIndex(pieceIndex) instanceof BendPiece){
                nextCorner = pieceIndex;
            }
        }
        return nextCorner;
    }

    /**
     * Return distance to start of given piece
     * @param currentPieceIndex
     * @param nextCornerIndex
     * @param inPieceDistance
     * @return
     */
    public double distanceToPiece(int currentPieceIndex, int nextCornerIndex, double inPieceDistance) {
        double distance;

        distance = trackLayout.get(currentPieceIndex).getLength() - inPieceDistance;

        for (int i = currentPieceIndex; i % trackLayout.size() != nextCornerIndex; i++){
            distance += getPieceAtIndex(i).getLength();
        }

        return distance;
    }
}
